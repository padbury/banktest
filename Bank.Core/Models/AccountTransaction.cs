﻿using NPoco;
using System;

namespace Bank.Core.Models
{
    public class AccountTransaction
    {
        public Guid Id { get; set; }
        public string AccountId { get; set; }
        public Money Amount { get; set; }
        public decimal ExchangeRate { get; set; }
        public Money DestinationAmount { get; set; }

        public string DestinationCurrencyCode { get; set; }
        public DateTime Created { get; set; }
    }
}
