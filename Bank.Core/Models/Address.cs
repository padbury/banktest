﻿namespace Bank.Core.Models
{
    public class Address
    {
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }

        public string Formatted
        {
            get
            {
                return $"{AddressLine2}, {Town}, {County}, {Postcode}, {Country}";
            }
        }
    }
}
