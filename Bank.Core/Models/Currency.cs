﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.Core.Models
{
    public class Currency
    {
        public string Code { get; set; }
        public string Symbol { get; set; }
    }
}
