﻿using Bank.Core.Models;
using System.Collections.Generic;

namespace Bank.Core.Services.Interfaces
{
    public interface ICustomerAccountService
    {
        CustomerAccount GetById(string id);
        bool AccountExists(CustomerAccount account);
        string AddAccount(CustomerAccount account);
        List<CustomerAccount> Find(string q, long pageIndex, int pageSize);
        void AddFunds(string accountId, Money amount, decimal exchangeRate = 1);
        void TransferMoney(MoneyTransfer moneyTransfer);
        Money GetAccountBalance(string accountId);
        List<CustomerAccount> GetAllAccounts(long pageIndex, int pageSize);
        string GetSortCode(string currencyCode);
        List<AccountTransaction> GetAllTransactions(string accountId);
    }
}
