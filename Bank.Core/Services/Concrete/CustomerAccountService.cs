﻿using Bank.Core.Mappers;
using Bank.Core.Models;
using Bank.Core.Persistence;
using Bank.Core.Services.Interfaces;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bank.Core.Services.Concrete
{
    public class CustomerAccountService : ICustomerAccountService
    {
        private readonly Configuration _settings;
        public CustomerAccountService(IOptions<Configuration> settings)
        {
            _settings = settings.Value;
        }

        /// <summary>
        /// Add an account.
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public string AddAccount(CustomerAccount account)
        {
            using (var db = new UnitOfWork(_settings.ConnectionString, true))
            {
                // Down and dirty way of generating a fake account number from a Guid. Not enough time to create a proper account code generator
                account.AccountNumber = Guid.NewGuid().ToString().Replace("-", ""); 

                // I'm using the SortCode + the Account Number as my unique id as the combination of both should be unique and it's far easier 
                // to deal with a single primary key than a compound key using the sort and account number everywhere.
                account.Id = $"{account.SortCode} {account.AccountNumber}";
                db.CustomerAccounts.Insert(account.Map());
                db.Commit();
                return account.Id;
            }

        }

        /// <summary>
        /// Check an account exists. I'm using the first, lastnames, the first line of the address and the postcode for this check
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public bool AccountExists(CustomerAccount account)
        {
            using (var db = new UnitOfWork(_settings.ConnectionString, true))
            {
                return db.CustomerAccounts.Exists(account.Firstname, account.Lastname, account.Address.AddressLine1, account.Address.Postcode);
            }
        }

        /// <summary>
        /// Basic search. Nothing special, just searches on a database. In a larger system this wouldn't be accessing the DB directly but would
        /// be using aggregated data (e.g. DB aggregation or lucene)
        /// </summary>
        /// <param name="q"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public List<CustomerAccount> Find(string q, long pageIndex, int pageSize)
        {
            using (var db = new UnitOfWork(_settings.ConnectionString, true))
            {
                return db.CustomerAccounts.Find(q, pageIndex, pageSize).Items.Select(x=>x.Map()).ToList();
            }
        }

        /// <summary>
        /// Get an account by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CustomerAccount GetById(string id)
        {
            using (var db = new UnitOfWork(_settings.ConnectionString, false))
            {
                var account = db.CustomerAccounts.GetById(id);
                if (account == null) return null;

                return account.Map();
            }

        }

        /// <summary>
        /// In a normal system you'd probably get a sort code based on a holding branch. That's beyond the scope of this example
        /// so I've just determined the sort code by the base currency.
        /// </summary>
        /// <param name="currencyCode"></param>
        /// <returns></returns>
        public string GetSortCode(string currencyCode)
        {
            switch (currencyCode)
            {
                case "GBP":
                    return "40-00-01";
                case "EUR":
                    return "50-00-02";
                case "USD":
                    return "60-00-03";
            }
            return "40-00-01";
        }

        /// <summary>
        /// Add funds to an account
        /// </summary>
        /// <param name="accountId">The full account id</param>
        /// <param name="amount">The Amount to deposit</param>
        /// <param name="exchangeRate">The current exchange rate for the destination currency. If deposit is the same currency as the account then this can be skipped</param>
        public void AddFunds(string accountId, Money amount, decimal exchangeRate = 1)
        {
            using (var db = new UnitOfWork(_settings.ConnectionString, true))
            {
                var sourceAccount = db.CustomerAccounts.GetById(accountId);

                if (sourceAccount == null) // Do not proceed if the account doesn't exist
                    throw new Exception("The account does not exist.");

                var sourceTransaction = new Persistence.Models.AccountTransaction
                {
                    Id= Guid.NewGuid(),
                    AccountId = accountId,
                    Amount = amount.Amount,
                    Created = DateTime.Now,
                    CurrencyCode = amount.CurrencyCode,
                    DestinationCurrencyCode = sourceAccount.CurrencyCode,
                    ExchangeRate = exchangeRate
                };

                db.AccountTransactions.Insert(sourceTransaction);
                db.Commit();

            }
        }

        /// <summary>
        /// Transfer Money from one account to another
        /// </summary>
        /// <param name="moneyTransfer"></param>
        public void TransferMoney(MoneyTransfer moneyTransfer)
        {
            using (var db = new UnitOfWork(_settings.ConnectionString, true))
            {
                var sourceAccount = db.CustomerAccounts.GetById(moneyTransfer.SourceAccount);

                if (sourceAccount == null) // Check account exists
                    throw new Exception("The account does not exist.");

                // Get balance and check the account has enough funds
                var currentBalance = GetAccountBalance(moneyTransfer.SourceAccount);
                if (currentBalance.Amount < (moneyTransfer.AmountToTransfer.Amount * moneyTransfer.ExchangeRateDestination))
                    throw new Exception("This account does not have sufficient funds for this transaction.");

                // Check the destination account exists
                var destinationAccount = db.CustomerAccounts.GetById(moneyTransfer.DestinationAccount);

                if (destinationAccount == null)
                    throw new Exception("The destination account does not exist.");

                // Add transaction to remove funds from source account. This is done as a DB transaction so that the transaction for the destination account is ensured to complete
                var sourceTransaction = new Persistence.Models.AccountTransaction
                {
                    Id = Guid.NewGuid(),
                    AccountId = moneyTransfer.SourceAccount,
                    Amount = -(moneyTransfer.AmountToTransfer.Amount),
                    Created = DateTime.Now,
                    CurrencyCode = moneyTransfer.AmountToTransfer.CurrencyCode,
                    DestinationCurrencyCode = sourceAccount.CurrencyCode,
                    ExchangeRate = moneyTransfer.ExchangeRateSource
                };

                db.AccountTransactions.Insert(sourceTransaction);

                // Add tranaction to add funds to destination account
                var destinationTransaction = new Persistence.Models.AccountTransaction
                {
                    AccountId = moneyTransfer.DestinationAccount,
                    Amount = moneyTransfer.AmountToTransfer.Amount,
                    Created = DateTime.Now,
                    CurrencyCode = moneyTransfer.AmountToTransfer.CurrencyCode,
                    DestinationCurrencyCode = sourceAccount.CurrencyCode,
                    ExchangeRate = moneyTransfer.ExchangeRateDestination
                };

                db.AccountTransactions.Insert(destinationTransaction);
                db.Commit();

            }
        }

        /// <summary>
        /// Get the account balance
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public Money GetAccountBalance(string accountId)
        {
            using (var db = new UnitOfWork(_settings.ConnectionString, false))
            {
                var account = db.CustomerAccounts.GetById(accountId);

                // Check the account exists
                if (account == null) throw new Exception("Account not found");

                // Get the transactions
                var transactions = db.AccountTransactions.GetTransactionsByAccountId(accountId);

                if (!transactions.Any()) return new Money(0m, account.CurrencyCode);

                // Add up the transactions and use the exchange rate to get it to the right value for the account. 
                // In a bigger system, I'd not do it like this as it's inefficient to get all the transactions and sum them here. Far better to do that 
                // on the database layer and just return the summed amount. 
                return new Money(transactions.Sum(x => x.ExchangeRate * x.Amount), account.CurrencyCode);
            }
        }

        /// <summary>
        /// Gets all accounts. It does accept pagination, though I've not used paging on this system. It's kind of muscle memory to add paging into these
        /// things even when I'm not using them.
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public List<CustomerAccount> GetAllAccounts(long pageIndex, int pageSize)
        {
            using (var db = new UnitOfWork(_settings.ConnectionString, false))
            {
                // Get the results then map then into a proper, not database entity. Trying not to leak the database past the service layer
                return db.CustomerAccounts.GetAll(pageIndex, pageSize).Items.Select(x => x.Map()).ToList();
            }
        }

        /// <summary>
        /// Gets the transactions for an account
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public List<AccountTransaction> GetAllTransactions(string accountId)
        {
            using (var db = new UnitOfWork(_settings.ConnectionString, false))
            {
                return db.AccountTransactions.GetTransactionsByAccountId(accountId).Select(x => x.Map()).ToList();
            }
        }
    }
}