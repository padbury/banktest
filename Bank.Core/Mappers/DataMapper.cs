﻿using Bank.Core.Models;

namespace Bank.Core.Mappers
{
    /// <summary>
    /// Maps Database entities to business objects, used in the form of an extension method for each entity.
    /// I could have used Automapper here but Automapper is a pain in the backside sometimes and it's quicker to roll my own
    /// </summary>
    public static class DataMapper
    {
        public static CustomerAccount Map(this Persistence.Models.CustomerAccount value)
        {
            return new CustomerAccount
            {
                Id = value.Id,
                Firstname = value.Firstname,
                Lastname = value.Lastname,
                SortCode = value.SortCode,
                AccountNumber = value.AccountNumber,
                Address = new Address
                {
                    AddressLine1 = value.AddressLine1,
                    AddressLine2 = value.AddressLine2,
                    Town = value.Town,
                    County = value.County,
                    Country = value.Country,
                    Postcode = value.Postcode
                }, 
                Currency = new Currency { Code = value.CurrencyCode},
                AccountType = value.AccountType    
            };
        }

        public static Persistence.Models.CustomerAccount Map(this CustomerAccount value)
        {
            return new Persistence.Models.CustomerAccount
            {
                Id = value.Id,
                Firstname = value.Firstname,
                Lastname = value.Lastname,
                SortCode = value.SortCode,
                AccountNumber = value.AccountNumber,
                AddressLine1 = value.Address.AddressLine1,
                AddressLine2 = value.Address.AddressLine2,
                Town = value.Address.Town,
                County = value.Address.County,
                Country = value.Address.Country,
                Postcode = value.Address.Postcode,
                CurrencyCode = value.Currency.Code,
                AccountType = value.AccountType
            };
        }

        public static AccountTransaction Map(this Persistence.Models.AccountTransaction value)
        {
            return new AccountTransaction
            {
                Id = value.Id,
                AccountId = value.AccountId,
                Created = value.Created,
                ExchangeRate = value.ExchangeRate,
                DestinationCurrencyCode = value.DestinationCurrencyCode,
                Amount = new Money(value.Amount, value.CurrencyCode)
            };
        }

        public static Persistence.Models.AccountTransaction Map(this AccountTransaction value)
        {
            return new Persistence.Models.AccountTransaction
            {
                Id = value.Id,
                AccountId = value.AccountId,
                Amount = value.Amount.Amount,
                CurrencyCode = value.Amount.CurrencyCode,
                ExchangeRate = value.ExchangeRate,
                DestinationCurrencyCode = value.DestinationCurrencyCode,
                Created = value.Created
            };
        }
    }
}
