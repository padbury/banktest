﻿using NPoco;
using System;

namespace Bank.Core.Persistence.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();
        IDatabase Db { get; }
        void Register(IRepository repository);
        T GetRepository<T>() where T : class;

    }
}
