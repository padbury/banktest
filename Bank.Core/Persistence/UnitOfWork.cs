﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Bank.Core.Persistence.Interfaces;
using Bank.Core.Persistence.Repositories;
using NLog;
using NPoco;

namespace Bank.Core.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private Transaction _transaction;
        private readonly Dictionary<string, IRepository> _repositories;
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// I'm being lazy here. There's really no need for me to expose the Repos here as they can be accessed natively from within the scope of the UoW. However
        /// it makes the code read better (and looks more like Entity Framework) which helps to understand the functionality better.
        /// </summary>
        private CustomerAccountRepository customerAccounts;
        public CustomerAccountRepository CustomerAccounts
        {
            get
            {
                if (customerAccounts == null)
                    customerAccounts = GetRepository<CustomerAccountRepository>();
                return customerAccounts;
            }
        }

        private AccountTransactionRepository accountTransactions;
        public AccountTransactionRepository AccountTransactions
        {
            get
            {
                if (accountTransactions == null)
                    accountTransactions = GetRepository<AccountTransactionRepository>();
                return accountTransactions;
            }
        }


        public UnitOfWork(string connectionString, bool useTransaction = false)
        {
            Db = new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            _repositories = new Dictionary<string, IRepository>();

            if (!useTransaction) return;
            _transaction = new Transaction(Db, System.Data.IsolationLevel.Unspecified);
        }

        public IDatabase Db { get; }

        public void Commit()
        {
            if (_transaction == null) return;

            try
            {
                _transaction.Complete();
            }
            catch (Exception exc)
            {
                _logger.Error(exc, "Transaction Error: " + exc.Message);
                Db.AbortTransaction();
            }
            finally
            {
                _transaction.Dispose();
                _transaction = null;
            }

        }

        public void Dispose()
        {
            if (_transaction == null) return;

            if (_transaction != null)
            {
                Db.AbortTransaction(); // kill anything left open.
                _transaction.Dispose();
            }

            if (Db != null)
            {
                Db.Dispose();
            }

        }

        public T GetRepository<T>() where T : class
        {
            // Use reflection to get and create the requested repository
            if (!_repositories.TryGetValue(typeof(T).Name, out IRepository rep))
            {
                rep = (T)Activator.CreateInstance(typeof(T), this) as IRepository;
            }
            return rep as T;
        }

        public void Register(IRepository repository)
        {
            if (!_repositories.ContainsKey(repository.GetType().Name))
                _repositories.Add(repository.GetType().Name, repository);
        }
    }
}
