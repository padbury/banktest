﻿using NPoco;
using System;

namespace Bank.Core.Persistence.Models
{
    [PrimaryKey("Id", AutoIncrement = false)]
    public class AccountTransaction
    {
        public Guid Id { get; set; }
        public string AccountId { get; set; }
        public decimal Amount { get; set; }
        public string CurrencyCode { get; set; }
        public decimal ExchangeRate { get; set; }
        public string DestinationCurrencyCode { get; set; }
        public DateTime Created { get; set; }
    }
}
