import React, { Component } from 'react';
import axios from 'axios';
import { Redirect } from "react-router-dom";

export class CreateAccount extends Component {
    static displayName = CreateAccount.name;

    constructor(props) {
        super(props);
        this.state = {
            errors: [],
            AccountId: '', Firstname: '', Lastname: '', AddressLine1: '', AddressLine2: '', Town: '', County: '', Postcode: '', Country: '', AccountType: 'Personal', CurrencyCode: 'GBP', Amount: 0
        };
    }

    formChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({ [nam]: val });
    }

    formAmountChangeHandler = (event) => {
        this.setState({ Amount: parseFloat(event.target.value) });
    }

    // Use API to lookup address. The API is limited to 20 requests per day (otherwise it costs money) so this test app will only allow 20 lookups per day
    lookupAddress = (event) => {
        event.preventDefault();
        axios.get(`https://api.getAddress.io/find/${this.state.Postcode}/${this.state.AddressLine1}?api-key=HwKW6Uke9kO1RIiuKMdgRA29588&expand=true`, this.state)
            .then((res) => {
                if (res.data.addresses.length > 0) { // if the address exists, get it. I'm assuming the correct address is the first one if multiples are returned. A larger app would make this a choice for the user.
                    this.setState({ AddressLine2: res.data.addresses[0].line_1 });
                    this.setState({ Town: res.data.addresses[0].town_or_city });
                    this.setState({ County: res.data.addresses[0].county });
                    this.setState({ Country: res.data.addresses[0].country });
                } else {
                    alert('Address not found');
                }
            })
            .catch((err) => {
                console.log(err);
                alert('Cannot use API at this time - possibly out of free credits, please enter your address manually')
            });
    }

    formSubmitHandler = (event) => {
        event.preventDefault();

        axios.post('/account/add', this.state)
            .then((res) => {
                 this.setState({ AccountId: res.data.id });
            })
            .catch((err) => {
                console.log(err);
                this.setState({ errors: err.response.data.errors });
            });
    }
    render() {
        if (this.state.AccountId.length > 0) {
            return <Redirect to={"/view-account/" + this.state.AccountId} />
        }

        return (

            <div>
                <h1>Create Account</h1>
                { this.state.errors &&
                    <ul className="error">{this.state.errors.map((err) => <li key={err}> {err}</li>)} </ul>}

                { this.state.AccountId.length > 0 &&
                    <h3>Account Created: {this.state.AccountId} </h3>}



                <form onSubmit={this.formSubmitHandler} className="form form-horizontal">
                    <div className="row">
                        <div className="col-md-12">
                            <label htmlFor="AddressLine1">Enter the customers name:</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6">

                            <input
                                type='text'
                                name='Firstname'
                                className="form-control"
                                placeholder="First name"
                                onChange={this.formChangeHandler}
                            />
                        </div>
                        <div className="col-md-6">
                            <input
                                type='text'
                                name='Lastname'
                                className="form-control"
                                placeholder="Last name"
                                onChange={this.formChangeHandler}
                            />
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-12">
                            <label htmlFor="AddressLine1">Enter the customers address:</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-4">
                            <div className="form-group">
                                <input
                                    type='text'
                                    name='AddressLine1'
                                    placeholder='Address'
                                    className="form-control"
                                    placeholder="House number"

                                    onChange={this.formChangeHandler}
                                />
                            </div>
                        </div>
                        <div className="col-md-4">  <div className="form-group">
                            <input
                                type='text'
                                name='Postcode'
                                className="form-control"
                                placeholder="Postcode"
                                onChange={this.formChangeHandler}
                            /> </div>
                        </div>
                        <div className="col-md-2">
                            <button className="btn btn-primary" onClick={this.lookupAddress}>Lookup</button>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <input
                                    type='text'
                                    name='AddressLine2'
                                    placeholder='Address'
                                    className="form-control"
                                    value={this.state.AddressLine2}
                                    onChange={this.formChangeHandler}
                                /></div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <input
                                    type='text'
                                    name='Town'
                                    placeholder='Town/City'
                                    className="form-control"
                                    value={this.state.Town}
                                    onChange={this.formChangeHandler}
                                /></div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <input
                                    type='text'
                                    name='County'
                                    placeholder='County'
                                    className="form-control"
                                    value={this.state.County}
                                    onChange={this.formChangeHandler}
                                /></div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group">
                                <input
                                    type='text'
                                    name='Country'
                                    placeholder='Country'
                                    className="form-control"
                                    value={this.state.Country}
                                    onChange={this.formChangeHandler}
                                /></div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">
                            <div className="form-group">
                                <select name='AccountType' onChange={this.formChangeHandler} value={this.state.AccountType} className="form-control">
                                    <option value="Personal">Personal</option>
                                    <option value="Company">Company</option>
                                    <option value="Other">Other</option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <label for="AddressLine1">Enter initial deposit:</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-2">
                            <div className="form-group">
                                <select name='CurrencyCode' onChange={this.formChangeHandler} value={this.state.CurrencyCode} className="form-control">
                                    <option value="GBP">&pound;</option>
                                    <option value="EUR">&euro;</option>
                                    <option value="USD">$</option>
                                </select>
                            </div>
                        </div>
                        <div className="col-md-4">  <div className="form-group">
                            <input
                                type='number'
                                name='Amount'
                                className="form-control"
                                placeholder="Amount"
                                value={this.state.Amount}
                                onChange={this.formAmountChangeHandler}
                            /> </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6">
                            <input className="btn btn-primary" type='submit' value="Create" />
                        </div>
                    </div>

                </form>


            </div>
        );
    }
}
