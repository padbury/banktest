﻿using System;

namespace Bank.Models.Requests
{
    public class MoneyTransferRequest
    {
        public string SourceAccount { get; set; }

        public string DestinationAccount { get; set; }

        public decimal Amount { get; set; }
        public string CurrencyCode { get; set; }

    }
}
