﻿using System;

namespace Bank.Models.Requests
{
    public class AddFundsRequest
    {
        public string AccountId { get; set; }

        public decimal Amount { get; set; }
        public string CurrencyCode { get; set; }

    }
}
