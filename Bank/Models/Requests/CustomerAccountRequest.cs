﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bank.Models.Requests
{
    public class CustomerAccountRequest
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }

        public string CurrencyCode { get; set; }
        public decimal Amount { get; set; }

        public string AccountType { get; set; }
    }
}
